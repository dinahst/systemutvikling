import { pool } from '../mysql-pool';
import { Fireworks } from './fireworks';

document.getElementById('Winner').style.visibility = 'hidden';

//Firework as a new object 

const container = document.querySelector('.fireworks');
const fireworks = new Fireworks(container, {
  rocketsPoint: 50,
      hue: { min: 0, max: 360 },
      speed: 0.5,
      acceleration: 1.05,
      friction: 0.95,
      gravity: 1.5,
      particles: 50,
      trace: 3,
      explosion: 5,
      autoresize: true,
      brightness: { 
        min: 50, 
        max: 80,
        decay: { min: 0.015, max: 0.03 }
      },
      mouse: { 
        click: false, 
        move: false, 
        max: 3 
      },
      boundaries: { 
        x: 50, 
        y: 50, 
        width: container.clientWidth, 
        height: container.clientHeight 
      },
      sound: {
        enable: true,
        files: [
          'explosion0.mp3',
          'explosion1.mp3',
          'explosion2.mp3'
        ],
        volume: { min: 1, max: 2 },
      }
});

//Reset main event

let reset = document.getElementById('reset');
reset.onclick = () => {
let confirmReset = confirm("Are you sure you want to reset?");
if(confirmReset == true){
  pool.query('DELETE FROM Matches');
  pool.query('ALTER TABLE Matches AUTO_INCREMENT = 1');
  alert('Successful reset. Reload page to see changes!');
}
};

//ROUND 1 OPTIONS AND ROUND 2 INPUT CHANGE ON ROUND 1 SCORE CHANGE
for (let i = 1; i <= 16; i++) {
  //Create score options from 0-2

  let score = [0, 1, 2];

  for (let j = 0; j < score.length; j++) {
    let optScore = document.createElement('option');
    optScore.innerText = score[j];
    document.getElementById('ScoreSelect' + i).appendChild(optScore);
    optScore.value = score[j];
  }

  document.getElementById('ScoreSelect' + i).onchange = () => {
    if (i <= 8) {
      if (
        document.getElementById('ScoreSelect' + i).value >
        document.getElementById('ScoreSelect' + (i + 8)).value
      ) {
        document.getElementById('TeamSelect' + i + '-' + (i + 8)).innerText =
          document.getElementById('TeamSelect' + i).innerText;
      } else if (
        document.getElementById('ScoreSelect' + i).value <
        document.getElementById('ScoreSelect' + (i + 8)).value
      ) {
        document.getElementById('TeamSelect' + i + '-' + (i + 8)).innerText =
          document.getElementById('TeamSelect' + (i + 8)).innerText;
      } else if (
        document.getElementById('ScoreSelect' + i).value ==
        document.getElementById('ScoreSelect' + (i + 8)).value
      ) {
        document.getElementById('TeamSelect' + i + '-' + (i + 8)).innerText = '';
      }
    } else if (i > 8) {
      if (
        document.getElementById('ScoreSelect' + i).value >
        document.getElementById('ScoreSelect' + (i - 8)).value
      ) {
        document.getElementById('TeamSelect' + (i - 8) + '-' + i).innerText =
          document.getElementById('TeamSelect' + i).innerText;
      } else if (
        document.getElementById('ScoreSelect' + i).value <
        document.getElementById('ScoreSelect' + (i - 8)).value
      ) {
        document.getElementById('TeamSelect' + (i - 8) + '-' + i).innerText =
          document.getElementById('TeamSelect' + (i - 8)).innerText;
      } else if (
        document.getElementById('ScoreSelect' + i).value ==
        document.getElementById('ScoreSelect' + (i - 8)).value
      ) {
        document.getElementById('TeamSelect' + (i - 8) + '-' + i).innerText = '';
      }
    }
  };
}

//ROUND 2 OPTIONS, AND ROUND 3 INPUT CHANGE ON ROUND 2 SCORE CHANGE

for (let i = 1; i <= 8; i++) {
  //Create score options from 0-2

  let score = [0, 1, 2];

  for (let j = 0; j < score.length; j++) {
    let optScore = document.createElement('option');
    optScore.innerText = score[j];
    document.getElementById('ScoreSelect' + i + '-' + (i + 8)).appendChild(optScore);
    optScore.value = score[j];
  }

  document.getElementById('ScoreSelect' + i + '-' + (i + 8)).onchange = () => {
    if (i % 2 == 1) {
      if (
        document.getElementById('ScoreSelect' + i + '-' + (i + 8)).value >
        document.getElementById('ScoreSelect' + (i + 1) + '-' + (i + 9)).value
      ) {
        document.getElementById('TeamSelectSemi' + i).innerText = document.getElementById(
          'TeamSelect' + i + '-' + (i + 8)
        ).innerText;
      } else if (
        document.getElementById('ScoreSelect' + i + '-' + (i + 8)).value <
        document.getElementById('ScoreSelect' + (i + 1) + '-' + (i + 9)).value
      ) {
        document.getElementById('TeamSelectSemi' + i).innerText = document.getElementById(
          'TeamSelect' + (i + 1) + '-' + (i + 9)
        ).innerText;
      } else if (
        document.getElementById('ScoreSelect' + i + '-' + (i + 8)).value ==
        document.getElementById('ScoreSelect' + (i + 1) + '-' + (i + 9)).value
      ) {
        document.getElementById('TeamSelectSemi' + i).innerText = '';
      }
    } else if (i % 2 == 0) {
      if (
        document.getElementById('ScoreSelect' + i + '-' + (i + 8)).value >
        document.getElementById('ScoreSelect' + (i - 1) + '-' + (i + 7)).value
      ) {
        document.getElementById('TeamSelectSemi' + (i - 1)).innerText = document.getElementById(
          'TeamSelect' + i + '-' + (i + 8)
        ).innerText;
      } else if (
        document.getElementById('ScoreSelect' + i + '-' + (i + 8)).value <
        document.getElementById('ScoreSelect' + (i - 1) + '-' + (i + 7)).value
      ) {
        document.getElementById('TeamSelectSemi' + (i - 1)).innerText = document.getElementById(
          'TeamSelect' + (i - 1) + '-' + (i + 7)
        ).innerText;
      } else if (
        document.getElementById('ScoreSelect' + i + '-' + (i + 8)).value ==
        document.getElementById('ScoreSelect' + (i - 1) + '-' + (i + 7)).value
      ) {
        document.getElementById('TeamSelectSemi' + (i - 1)).innerText = '';
      }
    }
  };
}

//ROUND 3 OPTIONS AND ROUND 4 INPUT CHANGE ON ROUND 3 SCORE CHANGE

for (let i = 1; i <= 8; i = i + 2) {
  //Create score options from 0-2

  let score = [0, 1, 2];

  for (let j = 0; j < score.length; j++) {
    let optScore = document.createElement('option');
    optScore.innerText = score[j];
    document.getElementById('ScoreSelectSemi' + i).appendChild(optScore);
    optScore.value = score[j];
  }

  document.getElementById('ScoreSelectSemi' + i).onchange = () => {
    if (i == 1 || i == 5) {
      if (
        document.getElementById('ScoreSelectSemi' + i).value >
        document.getElementById('ScoreSelectSemi' + (i + 2)).value
      ) {
        document.getElementById('TeamSelectFinal' + i).innerText = document.getElementById(
          'TeamSelectSemi' + i
        ).innerText;
      } else if (
        document.getElementById('ScoreSelectSemi' + i).value <
        document.getElementById('ScoreSelectSemi' + (i + 2)).value
      ) {
        document.getElementById('TeamSelectFinal' + i).innerText = document.getElementById(
          'TeamSelectSemi' + (i + 2)
        ).innerText;
      } else if (
        document.getElementById('ScoreSelectSemi' + i).value ==
        document.getElementById('ScoreSelectSemi' + (i + 2)).value
      ) {
        document.getElementById('TeamSelectFinal' + i).innerText = '';
      }
    } else if (i == 3 || i == 7) {
      if (
        document.getElementById('ScoreSelectSemi' + i).value >
        document.getElementById('ScoreSelectSemi' + (i - 2)).value
      ) {
        document.getElementById('TeamSelectFinal' + (i - 2)).innerText = document.getElementById(
          'TeamSelectSemi' + i
        ).innerText;
      } else if (
        document.getElementById('ScoreSelectSemi' + i).value <
        document.getElementById('ScoreSelectSemi' + (i - 2)).value
      ) {
        document.getElementById('TeamSelectFinal' + (i - 2)).innerText = document.getElementById(
          'TeamSelectSemi' + (i - 2)
        ).innerText;
      } else if (
        document.getElementById('ScoreSelectSemi' + i).value ==
        document.getElementById('ScoreSelectSemi' + (i - 2)).value
      ) {
        document.getElementById('TeamSelectFinal' + (i - 2)).innerText = '';
      }
    }
  };
}

//ROUND 4 OPTIONS

for (let i = 1; i <= 5; i = i + 4) {
  //Create score options from 0-2

  let score = [0, 1, 2];

  for (let j = 0; j < score.length; j++) {
    let optScore = document.createElement('option');
    optScore.innerText = score[j];
    document.getElementById('ScoreSelectFinal' + i).appendChild(optScore);
    optScore.value = score[j];
  }
}

//DISABLE PREVIOUS AND NEW ROUNDS CONSIDERING WHICH ROUND THE USER IS IN

pool.query('SELECT * FROM Matches', (error, matches) => {
  if (matches.length < 8) {
    for (let b = 1; b <= 16; b++) {
      document.getElementById('ScoreSelect' + b).setAttribute('disabled', 'disabled');
    }
  }

  if (matches.length < 12) {
    for (let b = 1; b <= 8; b++) {
      document.getElementById('ScoreSelect' + b + '-' + (b + 8)).setAttribute('disabled', 'disabled');
    }
  }

  if (matches.length < 14) {
    for (let b = 1; b <= 8; b = b + 2) {
      document.getElementById('ScoreSelectSemi' + b).setAttribute('disabled', 'disabled');
    }
  }

  if (matches.length < 15) {
    for (let b = 1; b <= 5; b = b + 4) {
      document.getElementById('ScoreSelectFinal' + b).setAttribute('disabled', 'disabled');
    }
  }

  if (matches.length >= 12) {
    for (let b = 1; b <= 16; b++) {
      document.getElementById('ScoreSelect' + b).setAttribute('disabled', 'disabled');
    }
  }

  if (matches.length >= 14) {
    for (let b = 1; b <= 8; b++) {
      document.getElementById('ScoreSelect' + b + '-' + (b + 8)).setAttribute('disabled', 'disabled');
    }
  }

  if (matches.length >= 15) {
    for (let b = 1; b <= 8; b = b + 2) {
      document.getElementById('ScoreSelectSemi' + b).setAttribute('disabled', 'disabled');
    }

    //Show winner if last winner_id is set

    if (matches[14].Winner_id != 0) {
      for (let b = 1; b <= 5; b = b + 4) {
        document.getElementById('ScoreSelectFinal' + b).setAttribute('disabled', 'disabled');
      }
      document.getElementById("Winner").style.visibility = 'visible';
      pool.query(
        'SELECT * FROM Matches INNER JOIN Team WHERE Matches.Winner_id = Team.id',
        (error, finalmatches) => {
          for (let final of finalmatches) {
            document.getElementById('Winner').innerText = 'Winner: ' + final.TeamName;
            document.getElementById('Winner').onclick = () =>{
              fireworks.start();
              setTimeout(() => {
                fireworks.stop();
              }, 10000);
            }
          }
        }
      );
    }
  }
});

// ROUND 1 SELECT TEAMNAMES

pool.query(
  'SELECT * FROM Matches INNER JOIN Team WHERE Matches.TeamId1 = Team.id AND Matches.Round=1',
  (error, teams) => {
    if (error) return console.error(error);

    if (teams.length != 0) {
      for (let i = 1; i <= 8; i++) {
        document.getElementById('TeamSelect' + i).innerText = teams[i - 1].TeamName;
        document.getElementById('ScoreSelect' + i).value = teams[i - 1].ScoreT1;
      }
    }
  }
);

pool.query(
  'SELECT * FROM Matches INNER JOIN Team WHERE Matches.TeamId2 = Team.id AND Matches.Round=1',
  (error, teams) => {
    if (error) return console.error(error);

    if (teams.length != 0) {
      for (let i = 9; i <= 16; i++) {
        document.getElementById('TeamSelect' + i).innerText = teams[i - 9].TeamName;
        document.getElementById('ScoreSelect' + i).value = teams[i - 9].ScoreT2;
      }
    }
  }
);

// ROUND 2 SELECT SCORES

for (let i = 1, j = 1; i <= 8, j <= 4; i = i + 2, j++) {
  pool.query(
    'SELECT * FROM Matches INNER JOIN Team WHERE Matches.TeamId1 = Team.id AND Matches.Round=2',
    (error, teams) => {
      if (error) return console.error(error);

      if (teams.length != 0) {
        document.getElementById('ScoreSelect' + i + '-' + (i + 8)).value = teams[j - 1].ScoreT1;
      }
    }
  );
}

for (let i = 2, j = 1; i <= 8, j <= 4; i = i + 2, j++) {
  pool.query(
    'SELECT * FROM Matches INNER JOIN Team WHERE Matches.TeamId2 = Team.id AND Matches.Round=2',
    (error, teams) => {
      if (error) return console.error(error);

      if (teams.length != 0) {
        document.getElementById('ScoreSelect' + i + '-' + (i + 8)).value = teams[j - 1].ScoreT2;
      }
    }
  );
}

// ROUND 3 SELECT SCORES

for (let i = 1, j = 1; i <= 5, j <= 2; i = i + 4, j++) {
  pool.query(
    'SELECT * FROM Matches INNER JOIN Team WHERE Matches.TeamId1 = Team.id AND Matches.Round=3',
    (error, teams) => {
      if (error) return console.error(error);

      if (teams.length != 0) {
        document.getElementById('ScoreSelectSemi' + i).value = teams[j - 1].ScoreT1;
      }
    }
  );
}

for (let i = 3, j = 1; i <= 7, j <= 2; i = i + 4, j++) {
  pool.query(
    'SELECT * FROM Matches INNER JOIN Team WHERE Matches.TeamId2 = Team.id AND Matches.Round=3',
    (error, teams) => {
      if (error) return console.error(error);

      if (teams.length != 0) {
        document.getElementById('ScoreSelectSemi' + i).value = teams[j - 1].ScoreT2;
      }
    }
  );
}

// ROUND 4 SELECT SCORES

pool.query(
  'SELECT * FROM Matches INNER JOIN Team WHERE Matches.TeamId1 = Team.id AND Matches.Round=4',
  (error, teams) => {
    if (error) return console.error(error);

    for (let team of teams) {
      if (teams.length != 0) {
        document.getElementById('ScoreSelectFinal' + 1).value = team.ScoreT1;
      }
    }
  }
);

pool.query(
  'SELECT * FROM Matches INNER JOIN Team WHERE Matches.TeamId2 = Team.id AND Matches.Round=4',
  (error, teams) => {
    if (error) return console.error(error);

    for (let team of teams) {
      if (teams.length != 0) {
        document.getElementById('ScoreSelectFinal' + 5).value = team.ScoreT2;
      }
    }
  }
);

// ROUND 2 SELECT TEAMNAMES

pool.query(
  'SELECT * FROM Matches LEFT JOIN Team ON Matches.Winner_id = Team.id WHERE Matches.Round=1',
  (error, teams) => {
    if (error) return console.error(error);

    if (teams.length != 0) {
      for (let i = 1; i <= 8; i++) {
        if (teams[i - 1].Winner_id != 0) {
          if (i % 2 == 1) {
            document.getElementById('TeamSelect' + i + '-' + (i + 8)).innerText =
              teams[i - 1].TeamName;
          }
          if (i % 2 == 0) {
            document.getElementById('TeamSelect' + i + '-' + (i + 8)).innerText =
              teams[i - 1].TeamName;
          }
        }
      }
    }
  }
);

//ROUND 3 SELECT TEAMNAMES

pool.query(
  'SELECT * FROM Matches LEFT JOIN Team ON Matches.Winner_id = Team.id WHERE Matches.Round=2',
  (error, teams) => {
    if (teams.length != 0) {
      for (let team of teams) {
        if (team.Winner_id != 0) {
          for (let i = 1; i <= 8; i = i + 2) {
            if (i == 1) {
              document.getElementById('TeamSelectSemi' + i).innerText = teams[i - 1].TeamName;
            } else if (i == 3) {
              document.getElementById('TeamSelectSemi' + i).innerText = teams[i - 2].TeamName;
            } else if (i == 5) {
              document.getElementById('TeamSelectSemi' + i).innerText = teams[i - 3].TeamName;
            } else if (i == 7) {
              document.getElementById('TeamSelectSemi' + i).innerText = teams[i - 4].TeamName;
            }
          }
        }
      }
    }
  }
);

//ROUND 4 SELECT TEAMNAMES

pool.query(
  'SELECT * FROM Matches LEFT JOIN Team ON Matches.Winner_id = Team.id WHERE Matches.Round=3',
  (error, teams) => {
    if (teams.length != 0) {
      for (let team of teams) {
        if (team.Winner_id != 0) {
          for (let i = 1; i <= 5; i = i + 4) {
            if (i == 1) {
              document.getElementById('TeamSelectFinal' + i).innerText = teams[i - 1].TeamName;
            } else if (i == 5) {
              document.getElementById('TeamSelectFinal' + i).innerText = teams[i - 4].TeamName;
            }
          }
        }
      }
    }
  }
);

let saveBtn = document.getElementById('save');

saveBtn.onclick = () => {
  for (let b = 1; b <= 16; b++) {
    if (b <= 8) {
      //Update score 1 in first round matches -> database
      pool.query(
        'UPDATE Matches INNER JOIN Team ON Matches.TeamId1 = Team.id SET ScoreT1=? WHERE Team.TeamName=? AND Matches.Round=1',
        [
          document.getElementById('ScoreSelect' + b).value,
          document.getElementById('TeamSelect' + b).innerText,
        ],
        (error) => {
          if (error) return console.error(error); // If error, show error in console (in red text) and return
        }
      );
    } else if (b >= 9) {
      //Update score 2 in first round matches -> database
      pool.query(
        'UPDATE Matches INNER JOIN Team ON Matches.TeamId2 = Team.id SET ScoreT2=? WHERE Team.TeamName=? AND Matches.Round=1',
        [
          document.getElementById('ScoreSelect' + b).value,
          document.getElementById('TeamSelect' + b).innerText,
        ],
        (error) => {
          if (error) return console.error(error); // If error, show error in console (in red text) and return
        }
      );
    }
  }

  for (let b = 1; b <= 8; b++) {
    if (b % 2 == 1) {
      //Update score 1 in second round matches -> database
      pool.query(
        'UPDATE Matches INNER JOIN Team ON Matches.TeamId1 = Team.id SET ScoreT1=? WHERE Team.TeamName=? AND Matches.Round=2',
        [
          document.getElementById('ScoreSelect' + b + '-' + (b + 8)).value,
          document.getElementById('TeamSelect' + b + '-' + (b + 8)).innerText,
        ],
        (error) => {
          if (error) return console.error(error); // If error, show error in console (in red text) and return
        }
      );
    } else if (b % 2 == 0) {
      //Update score 2 in second round matches -> database
      pool.query(
        'UPDATE Matches INNER JOIN Team ON Matches.TeamId2 = Team.id SET ScoreT2=? WHERE Team.TeamName=? AND Matches.Round=2',
        [
          document.getElementById('ScoreSelect' + b + '-' + (b + 8)).value,
          document.getElementById('TeamSelect' + b + '-' + (b + 8)).innerText,
        ],
        (error) => {
          if (error) return console.error(error); // If error, show error in console (in red text) and return
        }
      );
    }
  }

  for (let b = 1; b <= 8; b = b + 2) {
    if (b == 1 || b == 5) {
      //Update score 1 in third round matches -> database
      pool.query(
        'UPDATE Matches INNER JOIN Team ON Matches.TeamId1 = Team.id SET ScoreT1=? WHERE Team.TeamName=? AND Matches.Round=3',
        [
          document.getElementById('ScoreSelectSemi' + b).value,
          document.getElementById('TeamSelectSemi' + b).innerText,
        ],
        (error) => {
          if (error) return console.error(error); // If error, show error in console (in red text) and return
        }
      );
    } else if (b == 3 || b == 7) {
      //Update score 2 in third round matches -> database
      pool.query(
        'UPDATE Matches INNER JOIN Team ON Matches.TeamId2 = Team.id SET ScoreT2=? WHERE Team.TeamName=? AND Matches.Round=3',
        [
          document.getElementById('ScoreSelectSemi' + b).value,
          document.getElementById('TeamSelectSemi' + b).innerText,
        ],
        (error) => {
          if (error) return console.error(error); // If error, show error in console (in red text) and return
        }
      );
    }
  }

  for (let b = 1; b <= 5; b++) {
    if (b == 1) {
      //Update score 1 in last round matches -> database
      pool.query(
        'UPDATE Matches INNER JOIN Team ON Matches.TeamId1 = Team.id SET ScoreT1=? WHERE Team.TeamName=? AND Matches.Round=4',
        [
          document.getElementById('ScoreSelectFinal' + b).value,
          document.getElementById('TeamSelectFinal' + b).innerText,
        ],
        (error) => {
          if (error) return console.error(error); // If error, show error in console (in red text) and return
        }
      );
    } else if (b == 5) {
      //Update score 2 in last round matches -> database
      pool.query(
        'UPDATE Matches INNER JOIN Team ON Matches.TeamId2 = Team.id SET ScoreT2=? WHERE Team.TeamName=? AND Matches.Round=4',
        [
          document.getElementById('ScoreSelectFinal' + b).value,
          document.getElementById('TeamSelectFinal' + b).innerText,
        ],
        (error) => {
          if (error) return console.error(error); // If error, show error in console (in red text) and return
        }
      );
    }
  }

  //Calculate winner from both scores

  pool.query('SELECT * FROM Matches', (error, matches) => {
    if (error) return console.error(error);

    for (let match of matches) {
      if (match.ScoreT1 > match.ScoreT2) {
        pool.query(
          'UPDATE Matches SET Winner_id=? WHERE id=?',
          [match.TeamId1, match.id],
          (error) => {
            if (error) return console.error(error); // If error, show error in console (in red text) and return
          }
        );
      } else if (match.ScoreT1 < match.ScoreT2) {
        pool.query(
          'UPDATE Matches SET Winner_id=? WHERE id=?',
          [match.TeamId2, match.id],
          (error) => {
            if (error) return console.error(error); // If error, show error in console (in red text) and return
          }
        );
      } else if (match.ScoreT1 == match.ScoreT2) {
        pool.query('UPDATE Matches SET Winner_id=? WHERE id=?', [0, match.id], (error) => {
          if (error) return console.error(error); // If error, show error in console (in red text) and return
        });
      }
    }
  });

  alert('Successfully saved! Wait for a while and then reload to see changes!');

};

let nextRound = document.getElementById("nextRound");

nextRound.onclick = () => {

  let isExecuted = confirm("Do you want to enter a new round? !You will not be able to change previous rounds!");

if(isExecuted == true){

  //Inform user to save all scores before they can go to the next round

  pool.query(
    'SELECT * FROM Matches',
    (error, matches) => {
      if (matches.every((match) => match.Winner_id == 0)) {
        alert("You need to complete one round before you can enter a new round.");
      }
  });

   //New second round matches if all winner_ids are set in first round

  pool.query(
    'SELECT * FROM Matches LEFT JOIN Team ON Matches.Winner_id = Team.id WHERE Matches.Round=1',
    (error, matches) => {
      if (matches.every((match) => match.Winner_id != 0)) {
        if (matches.length == 8) {
          for (let i = 0; i <= 7; i = i + 2) {
            // Perform insert
            pool.query(
              'INSERT INTO Matches (TeamId1, TeamId2, ScoreT1, ScoreT2, Round, Winner_id) VALUES (?, ?, ?, ?, ?, ?)',
              [matches[i].Winner_id, matches[i + 1].Winner_id, 0, 0, 2, 0]
            );
          }
          for (let b = 1; b <= 16; b++) {
            document.getElementById('ScoreSelect' + b).setAttribute('disabled', 'disabled');
          }
          for (let b = 1; b <= 8; b++) {
              document.getElementById('ScoreSelect' + b + '-' + (b + 8)).removeAttribute('disabled');
          }
        } 
      } 
    }
  );

  //New third round matches if all winner_ids are set in second round

  pool.query(
    'SELECT * FROM Matches LEFT JOIN Team ON Matches.Winner_id = Team.id WHERE Matches.Round=2',
    (error, matches) => {
      if (error) return console.error(error);

      if (matches.every((match) => match.Winner_id != 0)) {
        if (matches.length == 4) {
          for (let i = 0; i <= 3; i = i + 2) {
            // Perform insert
            pool.query(
              'INSERT INTO Matches (TeamId1, TeamId2, ScoreT1, ScoreT2, Round, Winner_id) VALUES (?, ?, ?, ?, ?, ?)',
              [matches[i].Winner_id, matches[i + 1].Winner_id, 0, 0, 3, 0]
            );
          }
          for (let b = 1; b <= 8; b++) {
            document.getElementById('ScoreSelect' + b + '-' + (b + 8)).setAttribute('disabled', 'disabled');
          }
          for (let b = 1; b <= 8; b = b + 2) {
              document.getElementById('ScoreSelectSemi' + b).removeAttribute('disabled');
          }
        }
      }
    }
  );

//New last round matches if all winner_ids are set in third round

  pool.query(
    'SELECT * FROM Matches LEFT JOIN Team ON Matches.Winner_id = Team.id WHERE Matches.Round=3',
    (error, matches) => {
      if (error) return console.error(error);

      if (matches.every((match) => match.Winner_id != 0)) {
        if (matches.length == 2) {
          for (let i = 0; i <= 1; i = i + 2) {
            // Perform insert
            pool.query(
              'INSERT INTO Matches (TeamId1, TeamId2, ScoreT1, ScoreT2, Round, Winner_id) VALUES (?, ?, ?, ?, ?, ?)',
              [matches[i].Winner_id, matches[i + 1].Winner_id, 0, 0, 4, 0]
            );
          }
          for (let b = 1; b <= 8; b = b + 2) {
            document.getElementById('ScoreSelectSemi' + b).setAttribute('disabled', 'disabled');
          }
          for (let b = 1; b <= 5; b = b + 4) {
              document.getElementById('ScoreSelectFinal' + b).removeAttribute('disabled');
          }
        }
      }
    }
  );

  //Disable last round matches if all winner_ids are set in that round

  pool.query(
    'SELECT * FROM Matches LEFT JOIN Team ON Matches.Winner_id = Team.id WHERE Matches.Round=4',
    (error, matches) => {
      if (error) return console.error(error);

      if (matches.every((match) => match.Winner_id != 0)) {
        if(matches.length == 1){
          for (let b = 1; b <= 5; b = b + 4) {
            document.getElementById('ScoreSelectFinal' + b).setAttribute('disabled', 'disabled');
          }
        } 
      }
  });
}
}
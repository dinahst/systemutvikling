import { pool } from '../mysql-pool';

let newTeams = document.getElementById("NewteamSelect");
let groupNr = document.getElementById("groupSelect");
let oldTeams = document.getElementById("OldteamSelect");
let switchTeams1 = document.getElementById("SwitchteamSelect1");
let switchTeams2 = document.getElementById("SwitchteamSelect2");

//Reset group stage

let reset = document.getElementById('reset');
reset.onclick = () => {
let confirmReset = confirm("Are you sure you want to reset?");
if(confirmReset == true){
  pool.query('DELETE FROM GroupStage');
  pool.query('ALTER TABLE GroupStage AUTO_INCREMENT = 1');
  alert('Successful reset. Reload page to see changes!');
}
};

function newteamSelect(){

  while (newTeams.firstChild) {
    newTeams.removeChild(newTeams.firstChild);
  }

  let addDefault = document.createElement('option');
  addDefault.innerText = "<Select Team>";
  newTeams.appendChild(addDefault);

  //Create teamname options from team table
  pool.query('SELECT * FROM Team LEFT JOIN GroupStage ON Team.id = GroupStage.TeamId WHERE GroupStage.TeamId IS NULL;', (error, teams) => {
    if (error) return console.error(error);
    
    for (let team of teams) {
      let opt = document.createElement('option');
      opt.innerText = team.TeamName;   
      newTeams.appendChild(opt);
      opt.value = team.id;
    }
  });
}

newteamSelect();

function groupSelect(){

  while (groupNr.firstChild) {
    groupNr.removeChild(groupNr.firstChild);
  }

  let groupDefault = document.createElement('option');
  groupDefault.innerText = "<Select Group>";
  groupNr.appendChild(groupDefault);

   //Create group options
   for(let i = 1; i <= 8; i++){
    pool.query('SELECT * FROM GroupStage WHERE Groupnr=?', [i], (error, teams) => {
      if (error) return console.error(error);
        if(teams.length < 4) {
          let optGroup = document.createElement('option'); 
          optGroup.innerText = i;  
          groupNr.appendChild(optGroup);
        }
    });
   }

}

groupSelect();

let addBtn = document.getElementById("addBtn");

addBtn.onclick = () => {

  if(newTeams.value != "<Select Team>") {
    if(groupNr.value != "<Select Group>"){
      // Perform insert
      pool.query('INSERT INTO GroupStage (Groupnr, Score, TeamId) VALUES (?, ?, ?)', [
        document.getElementById("groupSelect").value,
        0,
        document.getElementById("NewteamSelect").value,
      ]);
        alert("Successfully added!"); 
        newteamSelect();
        groupSelect();
        oldteamSelect();
        switchSelect();
    } else{
      alert("Select a group number. If there are no remaining groups, remove a team from the group stage.");
    }
  } else {
      alert("Select a team first. If there are no teams, add more at the 'Teams and Players' page or remove an added team.");
  }

}

function oldteamSelect() {
  
  while (oldTeams.firstChild) {
    oldTeams.removeChild(oldTeams.firstChild);
  }

  let removeDefault = document.createElement('option');
  removeDefault.innerText = "<Select Team>";
  oldTeams.appendChild(removeDefault);

  //Create teamname options from team table
  pool.query('SELECT * FROM Team INNER JOIN GroupStage WHERE Team.id = GroupStage.TeamId', (error, teams) => {
    if (error) return console.error(error);
    
    for (let team of teams) {
      let opt = document.createElement('option');
      opt.innerText = team.TeamName;   
      oldTeams.appendChild(opt);
      opt.value = team.id;
    }
  });
}

oldteamSelect();

let removeBtn = document.getElementById("removeBtn");

removeBtn.onclick = () => {

  if(oldTeams.value != "<Select Team>") {
    //Delete row from team with id = team-Id
    pool.query('DELETE FROM GroupStage WHERE TeamId = ?', [oldTeams.value], (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return
    });
    alert("Successfully removed");
    newteamSelect();
    groupSelect();
    oldteamSelect();
    switchSelect();
  } else{
      alert("Select a team first!");
  }
}

function switchSelect(){

  while (switchTeams1.firstChild) {
    switchTeams1.removeChild(switchTeams1.firstChild);
  }

  while (switchTeams2.firstChild) {
    switchTeams2.removeChild(switchTeams2.firstChild);
  }

for(let i = 1; i <= 2; i++){

  let optDefault = document.createElement('option');
  optDefault.innerText = "<Select Team>";
  document.getElementById("SwitchteamSelect" + i).appendChild(optDefault);

  //Create teamname options from team table
  pool.query('SELECT * FROM Team INNER JOIN GroupStage WHERE Team.id = GroupStage.TeamId', (error, teams) => {
    if (error) return console.error(error);
    
    for (let team of teams) {

      let opt = document.createElement('option');
      opt.innerText = team.TeamName;   
      document.getElementById("SwitchteamSelect" + i).appendChild(opt);
      opt.value = team.id;
    }
  });
}

}

switchSelect();

for(let i = 1; i <= 2; i++){
document.getElementById("SwitchteamSelect" + i).onchange = () => {
  pool.query('SELECT * FROM GroupStage', (error, teams) => {
    if (error) return console.error(error);

      for (let team of teams) {

        if(document.getElementById("SwitchteamSelect" + i).value == team.TeamId){
        document.getElementById("SwitchteamGroup" + i).innerHTML = "Group: " + team.Groupnr;
        }
      }
    });
}
}

  let switchBtn = document.getElementById("switchBtn");

  //Switch selected teams from two different groups

  switchBtn.onclick = () => {

        if(switchTeams1.value != "<Select Team>" && switchTeams2.value != "<Select Team>"){

        if(document.getElementById("SwitchteamGroup1").innerHTML != document.getElementById("SwitchteamGroup2").innerHTML){

        pool.query('SELECT * FROM GroupStage', (error, teams) => {
          if (error) return console.error(error);
          
          for (let team of teams) {
          
          if(team.TeamId == switchTeams1.value){
            console.log(switchTeams1.value);
            pool.query(
              'UPDATE GroupStage SET Groupnr=? WHERE TeamId=?',
              [team.Groupnr, switchTeams2.value],
              (error) => {
                if (error) return console.error(error); // If error, show error in console (in red text) and return
              }
            );
          }

          if(team.TeamId == switchTeams2.value){
            console.log(switchTeams2.value);
            pool.query(
              'UPDATE GroupStage SET Groupnr=? WHERE TeamId=?',
              [team.Groupnr, switchTeams1.value],
              (error) => {
                if (error) return console.error(error); // If error, show error in console (in red text) and return
              }
            );
          }

        }});

        alert("Successfully changed.");
      
        } else{
          alert("Choose two different teams from different groups.");
        }

        } else{
            alert("Select teams in the dropdown menus!!");
        } 

      }


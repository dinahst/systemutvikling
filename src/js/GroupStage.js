import { pool } from '../mysql-pool';

//Move to edit page when button is clicked

let editbtn = document.getElementById('groupChange');

editbtn.onclick = () => {
  pool.query('SELECT * FROM Matches', (error, matches) => {
    if (error) return console.error(error);
    
  if(matches.length == 0){
    window.location.href = "./ChangeGroup.html";
  } else {
    alert("Data is already sent to Main Event. The Group Stage is done.");
  }
  });
}

for (let i = 1; i <= 32; i++) {
  document.getElementById('ScoreSelect' + i).setAttribute('disabled', 'disabled');

  //Create score options from 0-3

  let score = [0, 1, 2, 3];

  for (let j = 0; j < score.length; j++) {
    let optScore = document.createElement('option');
    optScore.innerText = score[j];
    document.getElementById('ScoreSelect' + i).appendChild(optScore);
    optScore.value = score[j];
  }
}

let group1 = [];
let score1 = [];
let group2 = [];
let score2 = [];
let group3 = [];
let score3 = [];
let group4 = [];
let score4 = [];
let group5 = [];
let score5 = [];
let group6 = [];
let score6 = [];
let group7 = [];
let score7 = [];
let group8 = [];
let score8 = [];

//SELECT ALL TEAMS AND SCORES FROM EACH GROUP

pool.query(
  'SELECT * FROM GroupStage INNER JOIN Team WHERE GroupStage.TeamId = Team.id',
  (error, groupTeams) => {
    if (error) return console.error(error);

    for (let team of groupTeams) {
      if (team.Groupnr == 1) {
        group1.push(team.TeamName);
        score1.push(team.Score);

        for (let i = 1; i <= 4; i++) {
          if (group1[i - 1] != undefined) {
            document.getElementById('Team' + i).innerText = group1[i - 1];
            document.getElementById('ScoreSelect' + i).value = score1[i - 1];
          }
        }
      }

      if (team.Groupnr == 2) {
        group2.push(team.TeamName);
        score2.push(team.Score);

        for (let i = 5; i <= 8; i++) {
          if (group2[i - 5] != undefined) {
            document.getElementById('Team' + i).innerText = group2[i - 5];
            document.getElementById('ScoreSelect' + i).value = score2[i - 5];
          }
        }
      }

      if (team.Groupnr == 3) {
        group3.push(team.TeamName);
        score3.push(team.Score);

        for (let i = 9; i <= 12; i++) {
          if (group3[i - 9] != undefined) {
            document.getElementById('Team' + i).innerText = group3[i - 9];
            document.getElementById('ScoreSelect' + i).value = score3[i - 9];
          }
        }
      }

      if (team.Groupnr == 4) {
        group4.push(team.TeamName);
        score4.push(team.Score);

        for (let i = 13; i <= 16; i++) {
          if (group4[i - 13] != undefined) {
            document.getElementById('Team' + i).innerText = group4[i - 13];
            document.getElementById('ScoreSelect' + i).value = score4[i - 13];
          }
        }
      }

      if (team.Groupnr == 5) {
        group5.push(team.TeamName);
        score5.push(team.Score);

        for (let i = 17; i <= 20; i++) {
          if (group5[i - 17] != undefined) {
            document.getElementById('Team' + i).innerText = group5[i - 17];
            document.getElementById('ScoreSelect' + i).value = score5[i - 17];
          }
        }
      }

      if (team.Groupnr == 6) {
        group6.push(team.TeamName);
        score6.push(team.Score);

        for (let i = 21; i <= 24; i++) {
          if (group6[i - 21] != undefined) {
            document.getElementById('Team' + i).innerText = group6[i - 21];
            document.getElementById('ScoreSelect' + i).value = score6[i - 21];
          }
        }
      }

      if (team.Groupnr == 7) {
        group7.push(team.TeamName);
        score7.push(team.Score);

        for (let i = 25; i <= 28; i++) {
          if (group7[i - 25] != undefined) {
            document.getElementById('Team' + i).innerText = group7[i - 25];
            document.getElementById('ScoreSelect' + i).value = score7[i - 25];
          }
        }
      }

      if (team.Groupnr == 8) {
        group8.push(team.TeamName);
        score8.push(team.Score);

        for (let i = 29; i <= 32; i++) {
          if (group8[i - 29] != undefined) {
            document.getElementById('Team' + i).innerText = group8[i - 29];
            document.getElementById('ScoreSelect' + i).value = score8[i - 29];
          }
        }
      }
    }
  }
);

let saveBtn = document.getElementById('save');
let teamId = [];

pool.query('SELECT * FROM GroupStage ORDER BY Groupnr', (error, groupTeams) => {
  if (error) return console.error(error);

  saveBtn.style.visibility = 'hidden';
  btn.style.visibility = 'hidden';

  for (let team of groupTeams) {
      teamId.push(team.TeamId);
  }

  //IF THERE ARE 32 TEAMS IN GROUPS, YOU CAN CHANGE SCORE AND SEND TO MAIN EVENT

  if (teamId.length == 32) {
    for (let i = 1; i <= 32; i++) {
      document.getElementById('ScoreSelect' + i).removeAttribute('disabled');
    }
    saveBtn.style.visibility = 'visible';
    btn.style.visibility = 'visible';
  } else if (teamId.length < 32) {
    for (let i = 1; i <= 32; i++) {
      document.getElementById('ScoreSelect' + i).value = 0;
    }
  }
});

saveBtn.onclick = () => {

  pool.query('SELECT * FROM Matches', (error, matches) => {
    if (error) return console.error(error);
  
  if(matches.length == 0){
  for (let j = 1; j <= 32; j++) {
    //Update score -> database
    pool.query(
      'UPDATE GroupStage INNER JOIN Team ON GroupStage.TeamId = Team.id SET GroupStage.Score=? WHERE Team.TeamName=?',
      [
        document.getElementById('ScoreSelect' + j).value,
        document.getElementById('Team' + j).innerText,
      ],
      (error, results) => {
        if (error) return console.error(error); // If error, show error in console (in red text) and return
      }
    );
  }

  alert('Successfully saved!');
} else{
  alert("Data is already sent to Main Event. The Group Stage is done.");
}

});
};

let btn = document.getElementById('send');

btn.onclick = () => {
  pool.query('SELECT * FROM Matches', (error, matches) => {
    if (error) return console.error(error);

  if(matches.length == 0){
  let isExecuted = confirm("Are you sure you want to send to the Main Event? !You can only do it once!");
  if (isExecuted == true){
  pool.query('DELETE FROM Matches');

  let Team1 = [];
  let Team2 = [];

  //Send to main event by pushing top 2 teams from group 1-4 in one array and from group 5-8 in another array

  for (let i = 1; i <= 8; i++) {
    pool.query(
      'SELECT * FROM GroupStage WHERE Groupnr=? ORDER BY Score DESC',
      [i],
      (error, groupTeams) => {
        if (error) return console.error(error);

        if (i >= 1 && i <= 4) {
          Team1.push(groupTeams[0].TeamId);
          Team1.push(groupTeams[1].TeamId);
        }

        if (i >= 5 && i <= 8) {
          Team2.push(groupTeams[0].TeamId);
          Team2.push(groupTeams[1].TeamId);
        }

        //RANDOMLY SORT THE ARRAYS

        Team1.sort(() => Math.random() - 0.5);
        Team2.sort(() => Math.random() - 0.5);

        console.log(Team1);
        console.log(Team2);

        //8 NEW MATCHES IN MAIN EVENT FROM BOTH ARRAYS

        if (Team1.length == 8 && Team2.length == 8) {
          for (let i = 0; i <= 7; i++) {
            // Perform insert
            pool.query(
              'INSERT INTO Matches (TeamId1, TeamId2, ScoreT1, ScoreT2, Round, Winner_id) VALUES (?, ?, ?, ?, ?, ?)',
              [Team1[i], Team2[i], 0, 0, 1, 0]
            );
          }
        }
      }
    );
  }

  alert('Successfully sent to Main Event!');
  }
  } else {
      alert("Data is already sent to Main Event! The Group Stage is done.");
    }
  });
};

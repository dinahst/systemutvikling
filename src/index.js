import { pool } from './mysql-pool';

let teamList = document.getElementById('teamList');
let nameInput = document.getElementById('nameInput');
let addbtn = document.getElementById('addButton');
let updateSelect = document.getElementById('updateSelect');
let playerSelect = document.getElementById('playerSelect');
let newName = document.getElementById('newNameInput');
let newPlayer = document.getElementById('newPlayerInput');

let idPlayer1 = document.getElementById('Player1');
let idPlayer2 = document.getElementById('Player2');
let idPlayer3 = document.getElementById('Player3');
let idPlayer4 = document.getElementById('Player4');
let idPlayer5 = document.getElementById('Player5');

playerSelect.style.display = 'none';

// Print the teams stored in the database to the console
function printteams() {
  // Remove dublicate elements in teamList:
  while (teamList.firstChild) {
    teamList.removeChild(teamList.firstChild);
  }

  // Perform select-query that fetches all the teams table rows from the database
  pool.query('SELECT * FROM Team', (error, teams) => {
    if (error) return console.error(error); // If error, show error in console (in red text) and return

    for (let team of teams) {
      let li = document.createElement('li');
      li.innerText = team.TeamName;

      let btn = document.createElement('button');
      btn.innerText = 'x';
      btn.style.color = 'white';
      btn.className = "btn";
      btn.style.backgroundColor = 'red';
      btn.onclick = () => {
        let isExecuted = confirm("Are you sure you want to delete this team?");

        if (isExecuted == true){
        //Delete row from team with id = team-Id
        pool.query('DELETE FROM Players WHERE TeamId = ?', [team.id], (error, results) => {
          if (error) return console.error(error); // If error, show error in console (in red text) and return
        });

        //Delete row from team with id = team-Id
        pool.query('DELETE FROM Team WHERE id = ?', [team.id], (error, results) => {
          if (error)
            return console.error(error), alert('The team is already signed up for at least one match in the tournament.'); // If error, show error in console (in red text) and return

          if (results.affectedRows > 0) {
            console.log('Successfully deleted team');
            playerSelect.style.display = 'none';
            printteams();
          }
        });
      }
      };
      li.appendChild(btn);

      teamList.appendChild(li);
    }
  });

  teamSelect();
}

printteams();

addbtn.onclick = () => {
  // Checks if input value is empty
  if (
    nameInput.value.length == 0 ||
    idPlayer1.value.length == 0 ||
    idPlayer2.value.length == 0 ||
    idPlayer3.value.length == 0 ||
    idPlayer4.value.length == 0 ||
    idPlayer5.value.length == 0
  ) {
    alert('Error! Name input can not be empty!');
  } else {
    // Perform insert
    pool.query('INSERT INTO Team (TeamName) VALUES (?)', [nameInput.value], (error, teams) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return

      console.log('Inserted id: ' + teams.insertId); // Print out the id of the inserted row'
      nameInput.value = '';

      for (let i = 1; i <= 5; i++) {
        // Perform insert
        pool.query('INSERT INTO Players (playerName, TeamId) VALUES (?, ?)', [
          document.getElementById('Player' + i).value,
          teams.insertId,
        ]);

        document.getElementById('Player' + i).value = '';
      }
    });
  }

  printteams();
};

function teamSelect() {
  // Remove dublicate elements in updateSelect:
  while (updateSelect.firstChild) {
    updateSelect.removeChild(updateSelect.firstChild);
  }

  let optDefault = document.createElement('option');
  optDefault.innerText = '<Select team>';
  updateSelect.appendChild(optDefault);

  //Create options from team table
  pool.query('SELECT * FROM Team', (error, teams) => {
    if (error) return console.error(error);

    for (let team of teams) {
      let opt = document.createElement('option');
      opt.innerText = team.TeamName;
      updateSelect.appendChild(opt);
      opt.value = team.id;
    }
  });

  updateSelect.onchange = () => {
    if (updateSelect.value == optDefault.value) {
      playerSelect.style.display = 'none';
    } else {
      playerSelect.style.display = 'block';
    }

    updatePlayers();
  };
}

function updatePlayers() {
  // Remove dublicate elements in playerSelect:
  while (playerSelect.firstChild) {
    playerSelect.removeChild(playerSelect.firstChild);
  }

  //Create options from player table
  pool.query('SELECT * FROM Players WHERE TeamId=?', [updateSelect.value], (error, players) => {
    if (error) return console.error(error);

    for (let player of players) {
      let option = document.createElement('option');
      option.innerText = player.playerName;
      playerSelect.appendChild(option);
      option.value = player.PlayerId;
    }
  });
}

document.getElementById('namebtn').onclick = () => {
  //checks if input is empty
  if (newName.value.length == 0) {
    alert('Error! Name input can not be empty!');
  } else {
    // Update team name
    pool.query(
      'UPDATE Team SET TeamName=? WHERE id=?',
      [newName.value, updateSelect.value],
      (error) => {
        if (error) return console.error(error); // If error, show error in console (in red text) and return

        console.log('Updated teamname');
        newName.value = '';
      }
    );
  }

  printteams();
};

//Update selected player

document.getElementById('playerbtn').onclick = () => {
  if (newPlayer.value.length == 0) {
    alert('Error! Name input can not be empty!');
  } else {
    pool.query(
      'UPDATE Players SET playerName=? WHERE PlayerId=?',
      [newPlayer.value, playerSelect.value],
      (error) => {
        if (error) return console.error(error); // If error, show error in console (in red text) and return

        console.log('Updated player');
        newPlayer.value = '';
      }
    );
  }

  updatePlayers();
};
